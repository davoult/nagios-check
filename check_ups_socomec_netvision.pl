#!/usr/bin/perl -w 
################ check_ups_socomec_netvision.pl ################
my $Version='0.1';
my $check_name="check_ups_socomec_netvision.pl";
# Date : Jun 18 2018
# Author  : Vincent FRICOU (vincent at fricouv dot eu)
# Nagios check to cover Socomec UPS.
################################################################

use POSIX qw(locale_h);
use strict;
use Net::SNMP;
use Getopt::Long;
use Switch;
use Number::Format qw(:subs);
use Date::Parse;
use Data::Dumper;

### Global vars declaration
my ($o_host,$o_community,$o_port,$o_help,$o_timeout,$o_warn,$o_crit,$o_type,$o_perf,$o_expect,$o_subtype,$o_iface);
my %reverse_exit_code = (
	'Ok'=>0,
	'Warning'=>1,
	'Critical'=>2,
	'Unknown'=>3,
);

my $output='';
my $perf='';
my $outputlines;
my $countcritical=0;
my $countwarning=0;
my ($snmpsession,$snmperror);

### OID vars declaration

my %OID_GEN = (
	'Model'					=> '.1.3.6.1.4.1.4555.1.1.1.1.1.1.0',
	'UPSFirmwareVersion'	=> '.1.3.6.1.4.1.4555.1.1.1.1.1.2.0',
	'UpsSerialNumber'		=> '.1.3.6.1.4.1.4555.1.1.1.1.1.4.0',
);

my %OID_BATTERY = (
	'Status'			=> '.1.3.6.1.4.1.4555.1.1.1.1.2.1.0',
	'SecondsOnBattery'	=> '.1.3.6.1.4.1.4555.1.1.1.1.2.2.0',
	'MinutesRemaining'	=> '.1.3.6.1.4.1.4555.1.1.1.1.2.3.0',
	'ChargeRemaining'	=> '.1.3.6.1.4.1.4555.1.1.1.1.2.4.0', # Percents
	'Voltage'			=> '.1.3.6.1.4.1.4555.1.1.1.1.2.5.0',
	'Temperature'		=> '.1.3.6.1.4.1.4555.1.1.1.1.2.6.0',
);

my %OID_INPUT = (
	'LineIdx'				=> '.1.3.6.1.4.1.4555.1.1.1.1.3.3.1.1',
	'Voltage'				=> '.1.3.6.1.4.1.4555.1.1.1.1.3.3.1.2',
	'Current'				=> '.1.3.6.1.4.1.4555.1.1.1.1.3.3.1.3',
);

my %OID_OUTPUT = (
	'LineIdx'				=> '.1.3.6.1.4.1.4555.1.1.1.1.4.4.1.1',
	'Voltage'				=> '.1.3.6.1.4.1.4555.1.1.1.1.4.4.1.2',
	'Current'				=> '.1.3.6.1.4.1.4555.1.1.1.1.4.4.1.3',
);

my %OID_ALARM = (
	'Present'				=> '.1.3.6.1.4.1.4555.1.1.1.1.6.1.0',
	'AlarmId'				=> '.1.3.6.1.4.1.4555.1.1.1.1.6.2.1.1',
	'Desc'					=> '.1.3.6.1.4.1.4555.1.1.1.1.6.2.1.2',
	'Time'					=> '.1.3.6.1.4.1.4555.1.1.1.1.6.2.1.3',
	'ExtDesc'				=> '.1.3.6.1.4.1.4555.1.1.1.1.6.2.1.4',
);

### Output traitment tables

my %rev_bat_status = (
	1	=> 'unknown',
	2	=> 'batteryNormal',
	3	=> 'batteryLow',
	4	=> 'batteryDepleted',
	5	=> 'batteryDischarging',
	6	=> 'batteryFailure',
	7	=> 'upsOff',
);

my %bat_status = (
	'unknown'				=> 1,
	'batteryNormal'			=> 2,
	'batteryLow'			=> 3,
	'batteryDepleted'		=> 4,
	'batteryDischarging'	=> 5,
	'batteryFailure'		=> 6,
	'upsOff'				=> 7,
);

### Function declaration
sub usage {
   print "\nSNMP '.$check_name.' for Nagios. Version ",$Version,"\n";
   print "GPL Licence - Vincent FRICOU\n\n";
   print <<EOT;
-h, --help
   print this help message
-H, --hostname=HOST
   name or IP address of host to check
-C, --community=COMMUNITY NAME
   community name for the host's SNMP agent (implies v1 protocol)
-f, --perfparse
   perfparse output
-w, --warning=<voltmin>,<voltmax>,<currentmin>,<currentmax>
   Warning value for types : input and output
-c, --critical=<voltmin>,<voltmax>,<currentmin>,<currentmax>
   Critical value for types : input and output
-T, --type=CHECK TYPE
EOT
   type_help();
   exit $reverse_exit_code{Unknown};
}

sub type_help {
print "Type :
	- information : Get generic information about UPS such as model name, firmware version and S/N
	- battery : Get informations about battery status, charge pct, voltage, remaining time, …
	- input : Get informations about input line voltage/current (Need warning/critical values)
	- output : Get informations about output line voltage/current (Need warning/critical values)
	- alarm : Get active alarms from UPS (Only information no state published)
";
	exit $reverse_exit_code{Unknown};
}
sub get_options () {
	Getopt::Long::Configure ("bundling");
	GetOptions(
		'h'		=>	\$o_help,		'help'			=>	\$o_help,
		'H:s'	=>	\$o_host,		'hostname:s'	=>	\$o_host,
		'C:s'	=>	\$o_community,	'community:s'	=>	\$o_community,
		'f'		=>	\$o_perf,		'perfparse'		=>	\$o_perf,
		'w:s'	=>	\$o_warn,		'warning:s'		=>	\$o_warn,
		'c:s'	=>	\$o_crit,		'critical:s'	=>	\$o_crit,
		'T:s'	=>	\$o_type,		'type:s'		=>	\$o_type,
	);

	usage() if (defined ($o_help));
	usage() if (! defined ($o_host) && ! defined ($o_community) && ! defined ($o_type));
	type_help() if (! defined ($o_type));
	$o_type=uc($o_type);
}

sub output_display () {
	$output =~ tr/,/\n/;
	if ($countcritical > 0){
		if ( $outputlines > 1 ) {
			print "Critical : Click for detail\n\n$output";
		} else {
			print "Critical - ".$output;
		}
		exit $reverse_exit_code{Critical};
	} elsif ($countwarning > 0){
		if ( $outputlines > 1 ) {
			print "Warning : Click for detail\n\n$output";
		} else {
			print "Warning - ".$output;
		}
		exit $reverse_exit_code{Warning};
	} else {
		print $output;
		exit $reverse_exit_code{Ok};
	}
}

sub get_perf_data () {
	$output=$output.'|'.$perf;
}

sub init_snmp_session () {
	my %snmpparms = (
		"-hostname"		=>	$o_host,
		"-version"		=>	1,
		"-community"	=>	$o_community,
	);
	($snmpsession,$snmperror) = Net::SNMP->session(%snmpparms);
	if (!defined $snmpsession) {
		printf "SNMP: %s\n", $snmperror;
		exit $reverse_exit_code{Unknown};
	}
}

sub check_alert_value () {
	if (! defined ($o_warn)) { print "Warning value missing, please use -w option.\n"; exit $reverse_exit_code{Unknown}; }
	if (! defined ($o_crit)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}
sub check_subtype () {
	if (! defined ($o_subtype)) { print "No subtype defined, please use -s option.\n"; exit $reverse_exit_code{Unknown}; }
}

get_options();

### Main

switch ($o_type) {
	case 'INFORMATION' {
		init_snmp_session ();
		my $upsIdentModel				= $snmpsession->get_request(-varbindlist => [$OID_GEN{'Model'}]);
		my $upsIdentUPSFirmwareVersion	= $snmpsession->get_request(-varbindlist => [$OID_GEN{'UPSFirmwareVersion'}]);
		my $upsIdentUpsSerialNumber		= $snmpsession->get_request(-varbindlist => [$OID_GEN{'UpsSerialNumber'}]);
		$output='Model : '.$$upsIdentModel{$OID_GEN{'Model'}}.'. Firmware : '.$$upsIdentUPSFirmwareVersion{$OID_GEN{'UPSFirmwareVersion'}}.'. S/N : '.$$upsIdentUpsSerialNumber{$OID_GEN{'UpsSerialNumber'}}.',';
	}
	case 'BATTERY' {
		init_snmp_session ();
		my $upsBatteryStatus				= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'Status'}]);
		my $upsSecondsOnBattery				= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'SecondOnBattery'}]);
		my $upsEstimatedMinutesRemaining	= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'MinutesRemaining'}]);
		my $upsEstimatedChargeRemaining		= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'ChargeRemaining'}]);
		my $upsBatteryVoltage				= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'Voltage'}]);
		my $upsBatteryTemperature			= $snmpsession->get_request(-varbindlist => [$OID_BATTERY{'Temperature'}]);

		if ($$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'batteryLow'} || $$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'upsOff'} || $$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'batteryFailure'} || $$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'batteryDepleted'}) {
			++$countcritical;
		}
		if ($$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'unknown'} || $$upsBatteryStatus{$OID_BATTERY{'Status'}} == $bat_status{'batteryDischarging'} ) {
			++$countwarning;
		}
		$output = $output.'Battery state : '.$rev_bat_status{$$upsBatteryStatus{$OID_BATTERY{'Status'}}}.'. ';

		if ($$upsEstimatedMinutesRemaining{$OID_BATTERY{'MinutesRemaining'}} > -1) {
			++$countwarning;
			$output = $output.'UPS working on battery,Remaining Time : '.$$upsEstimatedMinutesRemaining{$OID_BATTERY{'MinutesRemaining'}}.'min,';
			$perf = $perf.'remaining_time='.$$upsEstimatedMinutesRemaining{$OID_BATTERY{'MinutesRemaining'}}.';;,';
			$output = $output.'UPS on battery since '.$$upsSecondsOnBattery{$OID_BATTERY{'SecondsOnBattery'}}.'s,';
			$perf = $perf.'on_battery_time='.$$upsSecondsOnBattery{$OID_BATTERY{'SecondsOnBattery'}}.';;,';
		}
		$output = $output.'Estimated charge '.$$upsEstimatedChargeRemaining{$OID_BATTERY{'ChargeRemaining'}}.'%,';
		$perf = $perf.'estimated_charge='.$$upsEstimatedChargeRemaining{$OID_BATTERY{'ChargeRemaining'}}.';;,';
		$output = $output.'Battery Voltage '.$$upsBatteryVoltage{$OID_BATTERY{'Voltage'}} * 0.1.'V,';
		$perf = $perf.'battery_voltage='.$$upsBatteryVoltage{$OID_BATTERY{'Voltage'}} * 0.1.';;,';
		$output = $output.'Battery Temperature '.$$upsBatteryTemperature{$OID_BATTERY{'Temperature'}}.'°C,';
		$perf = $perf.'battery_temperature='.$$upsBatteryTemperature{$OID_BATTERY{'Temperature'}}.';;,';
	}
	case 'INPUT' {
		check_alert_value ();
		init_snmp_session ();
		my @warning= split (',', $o_warn);
		my @critical= split (',', $o_crit);
		if (scalar(@warning) < 4){ print "Missing value to warning. Please check help :\n"; usage(); }
		if (scalar(@critical) < 4){ print "Missing value to critical. Please check help:\n"; usage(); }
		my $upsInputLineIdx		= $snmpsession->get_entries(-columns => [$OID_INPUT{'LineIdx'}]);
		foreach my $LineIdx (keys %$upsInputLineIdx) {
			my $LineVoltage = $snmpsession->get_request(-varbindlist => [$OID_INPUT{'Voltage'}.'.'.$$upsInputLineIdx{$LineIdx}]);
			my $LineCurrent = $snmpsession->get_request(-varbindlist => [$OID_INPUT{'Current'}.'.'.$$upsInputLineIdx{$LineIdx}]);
			my $Voltage=$$LineVoltage{$OID_INPUT{'Voltage'}.'.'.$$upsInputLineIdx{$LineIdx}} * 0.1;
			my $Current=$$LineCurrent{$OID_INPUT{'Current'}.'.'.$$upsInputLineIdx{$LineIdx}} * 0.1;
			if ($Voltage <= $warning[0]) {
				if ($Voltage <= $critical[0]) {
					++$countcritical;
					$output = $output.'Critical - Input voltage on line '.$$upsInputLineIdx{$LineIdx}.' is to low '.$Voltage.'V ('.$critical[0].'V),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Input voltage on line '.$$upsInputLineIdx{$LineIdx}.' is to low '.$Voltage.'V ('.$warning[0].'V),';
				}
			} elsif ($Voltage >= $warning[1]) {
				if ($Voltage >= $critical[1]) {
					++$countcritical;
					$output = $output.'Critical - Input voltage on line '.$$upsInputLineIdx{$LineIdx}.' is to high '.$Voltage.'V ('.$critical[1].'V),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Input voltage on line '.$$upsInputLineIdx{$LineIdx}.' is to high '.$Voltage.'V ('.$warning[1].'V),';
				}
			} else {
				$output = $output.'Input voltage on line '.$$upsInputLineIdx{$LineIdx}.' is '.$Voltage.'V ('.$warning[0].'V < < '.$warning[1].'V),';
			}
			$perf = $perf.'line-'.$$upsInputLineIdx{$LineIdx}.'-voltage='.$Voltage.';'.$warning[0].':'.$warning[1].';'.$critical[0].':'.$critical[1].',';

			if ($Current <= $warning[2]) {
				if ($Current <= $critical[2]) {
					++$countcritical;
					$output = $output.'Critical - Input current on line '.$$upsInputLineIdx{$LineIdx}.' is to low '.$Current.'A ('.$critical[2].'A),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Input current on line '.$$upsInputLineIdx{$LineIdx}.' is to low '.$Current.'A ('.$warning[2].'A),';
				}
			} elsif ($Current >= $warning[3]) {
				if ($Current >= $critical[3]) {
					++$countcritical;
					$output = $output.'Critical - Input current on line '.$$upsInputLineIdx{$LineIdx}.' is to high '.$Current.'A ('.$critical[3].'A),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Input current on line '.$$upsInputLineIdx{$LineIdx}.' is to high '.$Current.'A ('.$warning[3].'A),';
				}
			} else {
				$output = $output.'Input current on line '.$$upsInputLineIdx{$LineIdx}.' is '.$Current.'A ('.$warning[2].'A < < '.$warning[3].'A),';
			}
			$perf = $perf.'line-'.$$upsInputLineIdx{$LineIdx}.'-current='.$Current.';'.$warning[2].':'.$warning[3].';'.$critical[2].':'.$critical[3].',';
			
		}
	}
	case 'OUTPUT' {
		check_alert_value ();
		init_snmp_session ();
		my @warning= split (',', $o_warn);
		my @critical= split (',', $o_crit);
		if (scalar(@warning) < 4){ print "Missing value to warning. Please check help :\n"; usage(); }
		if (scalar(@critical) < 4){ print "Missing value to critical. Please check help:\n"; usage(); }
		my $upsOutputLineIdx = $snmpsession->get_entries(-columns => [$OID_OUTPUT{'LineIdx'}]);
		foreach my $LineIdx (keys %$upsOutputLineIdx) {
			my $LineVoltage = $snmpsession->get_request(-varbindlist => [$OID_OUTPUT{'Voltage'}.'.'.$$upsOutputLineIdx{$LineIdx}]);
			my $LineCurrent = $snmpsession->get_request(-varbindlist => [$OID_OUTPUT{'Current'}.'.'.$$upsOutputLineIdx{$LineIdx}]);
			my $Voltage=$$LineVoltage{$OID_OUTPUT{'Voltage'}.'.'.$$upsOutputLineIdx{$LineIdx}} * 0.1;
			my $Current=$$LineCurrent{$OID_OUTPUT{'Current'}.'.'.$$upsOutputLineIdx{$LineIdx}} * 0.1;
			if ($Voltage <= $warning[0]) {
				if ($Voltage <= $critical[0]) {
					++$countcritical;
					$output = $output.'Critical - Output voltage on line '.$$upsOutputLineIdx{$LineIdx}.' is to low '.$Voltage.'V ('.$critical[0].'V),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Output voltage on line '.$$upsOutputLineIdx{$LineIdx}.' is to low '.$Voltage.'V ('.$warning[0].'V),';
				}
			} elsif ($Voltage >= $warning[1]) {
				if ($Voltage >= $critical[1]) {
					++$countcritical;
					$output = $output.'Critical - Output voltage on line '.$$upsOutputLineIdx{$LineIdx}.' is to high '.$Voltage.'V ('.$critical[1].'V),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Output voltage on line '.$$upsOutputLineIdx{$LineIdx}.' is to high '.$Voltage.'V ('.$warning[1].'V),';
				}
			} else {
				$output = $output.'Output voltage on line '.$$upsOutputLineIdx{$LineIdx}.' is '.$Voltage.'V ('.$warning[0].'V < < '.$warning[1].'V),';
			}
			$perf = $perf.'line-'.$$upsOutputLineIdx{$LineIdx}.'-voltage='.$Voltage.';'.$warning[0].':'.$warning[1].';'.$critical[0].':'.$critical[1].',';

			if ($Current <= $warning[2]) {
				if ($Current <= $critical[2]) {
					++$countcritical;
					$output = $output.'Critical - Output current on line '.$$upsOutputLineIdx{$LineIdx}.' is to low '.$Current.'A ('.$critical[2].'A),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Output current on line '.$$upsOutputLineIdx{$LineIdx}.' is to low '.$Current.'A ('.$warning[2].'A),';
				}
			} elsif ($Current >= $warning[3]) {
				if ($Current >= $critical[3]) {
					++$countcritical;
					$output = $output.'Critical - Output current on line '.$$upsOutputLineIdx{$LineIdx}.' is to high '.$Current.'A ('.$critical[3].'A),';
				} else {
					++$countwarning;
					$output = $output.'Warning - Output current on line '.$$upsOutputLineIdx{$LineIdx}.' is to high '.$Current.'A ('.$warning[3].'A),';
				}
			} else {
				$output = $output.'Output current on line '.$$upsOutputLineIdx{$LineIdx}.' is '.$Current.'A ('.$warning[2].'A < < '.$warning[3].'A),';
			}
			$perf = $perf.'line-'.$$upsOutputLineIdx{$LineIdx}.'-current='.$Current.';'.$warning[2].':'.$warning[3].';'.$critical[2].':'.$critical[3].',';
			
		}
	}
	case 'ALARM' {
		init_snmp_session ();
		my $upsAlarmPresent = $snmpsession->get_request(-varbindlist => [$OID_ALARM{'Present'}]);
		my $upsAlarmIdx = $snmpsession->get_entries(-columns => [$OID_ALARM{'AlarmId'}]);
		if ($$upsAlarmPresent{$OID_ALARM{'Present'}} > 0) {
			$output = $output.'Current alarm count '.$$upsAlarmPresent{$OID_ALARM{'Present'}}.',';
			foreach my $AlarmID (keys %$upsAlarmIdx) {
				my $AlarmDesc		= $snmpsession->get_request(-varbindlist => [$OID_ALARM{'Desc'}.'.'.$$upsAlarmIdx{$AlarmID}]);
				my $AlarmTime		= $snmpsession->get_request(-varbindlist => [$OID_ALARM{'Time'}.'.'.$$upsAlarmIdx{$AlarmID}]);
				my $AlarmExtDesc	= $snmpsession->get_request(-varbindlist => [$OID_ALARM{'ExtDesc'}.'.'.$$upsAlarmIdx{$AlarmID}]);
				$output = $output.'Alarm '.$$upsAlarmIdx{$AlarmID}.' started from '.$$AlarmTime{$OID_ALARM{'Time'}.'.'.$$upsAlarmIdx{$AlarmID}}.' with message '.$$AlarmDesc{$OID_ALARM{'Desc'}.'.'.$$upsAlarmIdx{$AlarmID}}.',';
				$output = $output.'Extended information '.$$AlarmExtDesc{$OID_ALARM{'ExtDesc'}.'.'.$$upsAlarmIdx{$AlarmID}}.',';
			}
		} else {
			$output = $output.'No alarm found,';
		}
	}
	else { $output = 'Type not recognized.'; }
}

$snmpsession->close;
$output =~ tr/,/\n/;
$perf =~ tr/' '/'_'/;
$outputlines = $output =~ tr/\n//;
get_perf_data() if (defined $o_perf && $o_perf ne '');
output_display();