# README

This repository containt custom nagios checks.

## Templates
You could find templates in [template folder](./template) to get a base to write new checks.

## TODO
**check\_snmp\_compellent.pl**
  * ~~Global Generic Status~~
  * ~~Controller Global State~~
  * ~~Disks Global State~~
  * ~~Enclosure Global State~~
  * ~~Controller Temperature~~
  * Enclosure Power Supply
  * Enclosure Temperature
  * Disk Folder State
  * Volumes State
  * Connected Server
  * Cache Life
  * Capacity Performance