#!/bin/bash

#USER='root'
#PASSWD='IneoInf1415'
#FILE='/etc/dynamic/image_i0_overlay_maskwindows.conf'

export LANG="fr_FR.UTF-8"

usage() {
echo "Usage :check_axis_getmask.sh
        -u Camera username
        -p Camera password
        -H Host target
        -c Normal mask count
	-f File to check"
exit 2
}

out() {
	if [ `echo $OUTPUT | tr ',' '\n' | wc -l` -gt 1 ] ;then
		if [ $COUNTWARNING -gt 0 ];then
			echo "Warning : click for details,"
			echo -n "$OUTPUT," | tr ',' '\n'
			echo " | ConfiguredMask=$CONFMASK;;$NMASKCOUNT"
			rm -f /tmp/check_axis_getmask/Cam_$HOSTTARGET/mask_$HOSTTARGET.conf 
			exit 1
		fi
	fi
	echo -n "Ok : $OUTPUT," | tr ',' '\n'
	echo " | ConfiguredMask=$CONFMASK;;$NMASKCOUNT"
	rm -f /tmp/check_axis_getmask/Cam_$HOSTTARGET/mask_$HOSTTARGET.conf 
	exit 0
}
 

ARGS=`echo $@ |sed -e 's:-[a-Z] :\n&:g' | sed -e 's: ::g'`

if [ "${USERNAME}" = "" ]; then echo "Username requiered (-u)"; exit 2; fi
if [ "${PASSWORD}" = "" ]; then echo "Password requiered (-p)"; exit 2; fi
if [ "${HOSTTARGET}" = "" ]; then echo "Host target requiered (-H)"; exit 2; fi
if [ "${NMASKCOUNT}" = "" ]; then echo "Normal mask number required (-c)"; exit 2;fi
if [ "${FILE}" = "" ]; then echo "File to check required (-f)"; exit 2;fi

W_DIRECTORY=/tmp/check_axis_getmask/Cam_$HOSTTARGET
if [ ! -d $W_DIRECTORY ]; then
	mkdir -p $W_DIRECTORY
fi

cd $W_DIRECTORY
ncftpget ftp://$USERNAME:$PASSWORD@$HOSTTARGET$FILE 2&> /dev/null 

if [ -a mask_$HOSTTARGET.ref.conf ]; then
	mv image_i0_overlay_maskwindows.conf mask_$HOSTTARGET.conf
else
	OUTPUT="No reference to compare. I create it"
	mv image_i0_overlay_maskwindows.conf mask_$HOSTTARGET.ref.conf
	out
fi
		
if [ -n `diff -ruN mask_$HOSTTARGET.conf mask_$HOSTTARGET.ref.conf` ]; then
	OUTPUT="No differences in configuration"
else
	OUTPUT="Configurations is different"
	out
fi

CONFMASK=`cat /tmp/check_axis_getmask/Cam_$HOSTTARGET/mask_$HOSTTARGET.conf | grep MaskWindows.M| wc -l`
if [ ! $CONFMASK -eq $NMASKCOUNT ]; then
	COUNTWARNING=1
	OUTPUT="$OUTPUT,active mask $CONFMASK ; normal declared mask $NMASKCOUNT"
	out
else 
	OUTPUT="$OUTPUT"
	out
fi
